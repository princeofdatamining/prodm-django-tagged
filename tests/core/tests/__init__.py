from django.test import TestCase
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User, Group
from tagged.models import Tagged, Item

from core import *

class RelationTest(TestCase):

    def setUp(self):
        self.u1, self.u2, self.it = build_user()
        build_tagged(self.u1, self.u2, self.it)
        build_relation()
        self.ct_user = ContentType.objects.get_for_model(self.u1)

    def test_foreign(self):
        for t in Tagged.filter(user=self.u1):
            # TODO: Tagged.filter(user=?) ==> SQL where with content_type_id = ?
            # self.assertEqual(self.ct_user, t.content_type)
            self.assertEqual(str(self.u1.pk), str(t.object_id))

    def test_foreign_filter(self):
        Tagged.filter(user__username="")

    def test_foreign_attr(self):
        t = Tagged.get(user=self.u1, scene='region')
        self.assertEqual(self.u1, t.user)
        self.assertEqual(None, t.group)

    def test_one_to_one(self):
        results = User.objects.filter(region__name='Guangzhou')
        self.assertEqual(1, len(results))
        self.assertEqual(self.u1, results[0])

    def test_one_to_many(self):
        results = User.objects.filter(regions__name='China')
        self.assertEqual(2, len(results))
