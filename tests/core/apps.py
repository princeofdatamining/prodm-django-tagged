from django.apps import AppConfig, apps

class CoreAppConfig(AppConfig):

    name = 'core'

    def ready(self):
        try:
            self.build_data()
        except:
            pass

    def build_data(self):
        from core import build_user, build_tagged
        build_tagged(*build_user())
