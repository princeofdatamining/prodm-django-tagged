
default_app_config = 'core.apps.CoreAppConfig'

from django.db import models

def build_user():
    from django.contrib.auth.models import User, Group
    #
    root, created = User.objects.get_or_create(username='root')
    user, _ = User.objects.get_or_create(username='adam')
    camp, _ = Group.objects.get_or_create(pk=root.pk)
    #
    return root, user, camp

def build_tagged(u1, u2, it):
    from tagged.models import Tagged
    Tagged.register(u1, 'region', 'Guangzhou')
    Tagged.register(u1, 'regions', 'China')
    Tagged.register(u1, 'regions', 'Guangdong')
    Tagged.register(u1, 'regions', 'Guangzhou')
    #
    Tagged.register(u2, 'region', 'Chengdu')
    Tagged.register(u2, 'regions', 'China')
    Tagged.register(u2, 'regions', 'Guangdong')
    Tagged.register(u2, 'regions', 'Guangzhou')
    #
    Tagged.register(u1, 'camp', 'Chaotic')
    Tagged.register(u2, 'camp', 'Netrual')
    Tagged.register(it, 'camp', 'Chaotic')

def build_relation():
    from django.contrib.auth.models import User, Group
    from tagged.models import Tagged, Item
    #
    from generic_relation.related_foreign import RelatedForeignKey
    from generic_relation.reverse_generic import GenericOneToOne, GenericOneToMany
    #
    RelatedForeignKey(User, cache_name='content', on_delete=models.CASCADE).contribute_to_class(Item, 'user')
    RelatedForeignKey(Group,cache_name='content', on_delete=models.CASCADE).contribute_to_class(Item, 'group')
    #
    GenericOneToOne(Item, extra_filters=dict(scene='region')).contribute_to_class(User, 'region')
    GenericOneToMany(Item, extra_filters=dict(scene='regions')).contribute_to_class(User, 'regions')

'''
In [7]: Tagged.filter(user=u1)
SELECT "tagged_item".* FROM "tagged_item" WHERE ("tagged_item"."object_id" = 1);
Out[7]: [<Item: Guangzhou>, <Item: China>, <Item: Guangdong>, <Item: Guangzhou>, <Item: Chaotic>, <Item: Chaotic>]
### TODO: WHERE ("tagged_item"."content_type_id" = ? AND "tagged_item"."object_id" = ?)

In [13]: Tagged.filter(user__username='root', scene='region')
### TODO: Error

In [13]: t = Tagged.get(user=u1, scene='region')
In [15]: t.user, t.group
Out[15]: (<User: root>, None)

######

In [7]: User.objects.filter(region__name='Guangzhou')
SELECT "auth_user"."id", "auth_user"."password", "auth_user"."last_login", "auth_user"."is_superuser", "auth_user"."username", "auth_user"."first_name", "auth_user"."last_name", "auth_user"."email", "auth_user"."is_staff", "auth_user"."is_active", "auth_user"."date_joined" 
FROM "auth_user" 
INNER JOIN "tagged_item" ON ("auth_user"."id" = "tagged_item"."object_id" AND (("tagged_item"."content_type_id" = 4 AND "tagged_item"."scene" = 'region'))) 
WHERE "tagged_item"."name" = 'Guangzhou' 
LIMIT 21; 
args=(4, 'region', 'Guangzhou')
Out[7]: [<User: root>]

In [8]: User.objects.filter(regions__name='China')
SELECT "auth_user"."id", "auth_user"."password", "auth_user"."last_login", "auth_user"."is_superuser", "auth_user"."username", "auth_user"."first_name", "auth_user"."last_name", "auth_user"."email", "auth_user"."is_staff", "auth_user"."is_active", "auth_user"."date_joined" 
FROM "auth_user" 
INNER JOIN "tagged_item" ON ("auth_user"."id" = "tagged_item"."object_id" AND (("tagged_item"."content_type_id" = 4 AND "tagged_item"."scene" = 'regions'))) 
WHERE "tagged_item"."name" = 'China' 
LIMIT 21; 
args=(4, 'regions', 'China')
Out[8]: [<User: root>, <User: admin>]
'''