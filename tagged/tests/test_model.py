from django.test import TestCase
from django.conf import settings
from django.apps import apps

User = apps.get_model(settings.AUTH_USER_MODEL)

def force_user(username, **kwargs):
    u, _ = User.objects.get_or_create(defaults=kwargs, username=username)
    return u

from .. import models, constant

BOOKMARK = 'Bookmark'
PYTHON = 'Python'
DJANGO = 'Django'
GOLANG = 'Golang'

INTEREST = 'Interest'
TRAVEL = 'Travel'
READ = 'Reading'
SPORTS = 'Sports'

class TestModel(TestCase):

    def setUp(self):
        models.Options.get_or_create(scene=BOOKMARK, name=PYTHON)
        models.Options.get_or_create(scene=BOOKMARK, name=DJANGO)
        models.Options.get_or_create(scene=BOOKMARK, name=GOLANG)
        #
        models.Options.get_or_create(scene=INTEREST, name=TRAVEL)
        models.Options.get_or_create(scene=INTEREST, name=READ)
        models.Options.get_or_create(scene=INTEREST, name=SPORTS)
        ##
        self.u1 = force_user('U001')
        self.u2 = force_user('U002')
        self.u3 = force_user('U003')

    def check_tagged(self, content, values, **kwargs):
        tagged = [t.name for t in models.Tagged.filter_content(content=content, **kwargs)]
        for v in values:
            self.assertIn(v, tagged)

    def test_simple(self):
        self.check_tagged(self.u1, [], scene=INTEREST)
        #
        models.Tagged.register(self.u1, INTEREST, TRAVEL)
        models.Tagged.register(self.u1, INTEREST, SPORTS)
        self.check_tagged(self.u1, [TRAVEL, SPORTS], scene=INTEREST)
        #
        models.Tagged.register(self.u1, INTEREST, TRAVEL)
        models.Tagged.register(self.u1, INTEREST, SPORTS, toggle=True)
        self.check_tagged(self.u1, [TRAVEL], scene=INTEREST)
        #
        models.Tagged.register(self.u1, INTEREST, TRAVEL, delete=True)
        models.Tagged.register(self.u1, INTEREST, SPORTS, toggle=True)
        self.check_tagged(self.u1, [SPORTS], scene=INTEREST)
        #