from django.db import models
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from . import constant

class OptionManager(models.Manager):

    def public(self, *args, queryset=None, **kwargs):
        return (self if queryset is None else queryset).filter(*args, **kwargs)

Options = OptionManager()

class Option(models.Model):

    class Meta:
        verbose_name = constant.MODEL_OPTION
        verbose_name_plural = constant.MODEL_OPTIONS
        unique_together = ('scene', 'name')

    objects = Options

    scene = models.CharField(max_length=64)
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name

#

class TaggedManager(models.Manager):

    def register(self, content, scene, name, delete=None, toggle=None):
        filters = dict(
            scene=scene, name=name,
            content_type=ContentType.objects.get_for_model(content), object_id=content.pk,
        )
        qs = self.filter(**filters)
        if delete or (toggle and qs):
            qs.delete()
        else:
            self.get_or_create(**filters)

    def filter_content(self, content=None, **kwargs):
        if content:
            kwargs['content_type'] = ContentType.objects.get_for_model(content)
            kwargs['object_id'] = content.pk
        return self.filter(**kwargs)

Tagged = TaggedManager()

class Item(models.Model):

    class Meta:
        verbose_name = constant.MODEL_TAGGED
        verbose_name_plural = constant.MODEL_TAGGED

    objects = Tagged

    # option = models.ForeignKey(Option)
    scene = models.CharField(max_length=64)
    name = models.CharField(max_length=64)
    #
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.CharField(max_length=191)
    content = GenericForeignKey()

    def __str__(self): return self.name

#
