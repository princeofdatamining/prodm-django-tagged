from django.utils.translation import ugettext_lazy as _, ungettext_lazy, pgettext_lazy, npgettext_lazy

APP_VERBOSE_NAME = 'tagged'

MODEL_OPTION = pgettext_lazy('Tagged', 'Option')
MODEL_OPTIONS = pgettext_lazy('Tagged', 'Options')

MODEL_TAGGED = pgettext_lazy('Tagged', 'Item')
